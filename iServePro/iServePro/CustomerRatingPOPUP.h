//
//  CustomerRatingPOPUP.h
//  iServePro
//
//  Created by Rahul Sharma on 03/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ratingPopDelegate <NSObject>

- (void)popUpRatingOFfDismiss:(NSString *)tag;

@end

@interface CustomerRatingPOPUP : UIView<UIGestureRecognizerDelegate>
{
    NSInteger reason;
}
@property (weak, nonatomic) id<ratingPopDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (assign, nonatomic)BOOL  isSelected;
- (IBAction)submitTheRating:(id)sender;

-(void) onWindow:(UIWindow *)onWindow;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOfContent;
@property (strong, nonatomic) IBOutlet UITableView *cancelTableView;
@property (strong, nonatomic) NSMutableArray *cancelReasons;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (assign, nonatomic) NSInteger selectedIndex;
@end
