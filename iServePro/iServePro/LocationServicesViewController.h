//
//  LocationServicesViewController.h
//  Roadyo
//
//  Created by Rahul Sharma on 5/12/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationServicesViewController : UIViewController
- (IBAction)navigateSetting:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *clickHere;
@end
