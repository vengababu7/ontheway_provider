//
//  collectionTableViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 22/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface collectionTableViewCell : UITableViewCell
@property (nonatomic, strong)NSMutableArray *images;
-(void)reloadCollectionView:(NSMutableArray*)jobImages;
-(void)reloadCollection:(NSInteger)jobImages;
-(void)addImagesSelected;
@property (strong, nonatomic) IBOutlet UICollectionView *jobCollection;
@property(assign, nonatomic)NSInteger count;
@property(assign, nonatomic)NSInteger count1;

@property (nonatomic, strong)NSMutableArray *UpdatedImages;

@property(assign, nonatomic)BOOL isDeleteActive;
@property(assign, nonatomic)NSInteger deletedIndexpath;
@property (nonatomic, strong)UIButton *buttonDelete;

@property(assign, nonatomic)BOOL isAddOrDelete;
@property (nonatomic, strong)NSMutableArray *cacheImages;

@end
