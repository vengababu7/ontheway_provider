
//
//  CitySelectTableViewCell.m
//  iServePro
//
//  Created by Rahul Sharma on 26/04/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CitySelectTableViewCell.h"

@implementation CitySelectTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
