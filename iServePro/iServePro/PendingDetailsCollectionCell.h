//
//  PendingDetailsCollectionCell.h
//  MelikeyPro
//
//  Created by Rahul Sharma on 03/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PendingDetailsCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *appointmentImage;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
