//
//  ScheduleTableCell.h
//  Onthewaypro
//
//  Created by Rahul Sharma on 02/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *status;
@property (strong, nonatomic) IBOutlet UILabel *startTime;
@property (strong, nonatomic) IBOutlet UILabel *endTime;
@end
