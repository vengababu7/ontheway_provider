//
//  JobTableViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 13/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *startTime;

@property (strong, nonatomic) IBOutlet UILabel *endDate;
@property (strong, nonatomic) IBOutlet UILabel *noofbookings;
@property (strong, nonatomic) IBOutlet UILabel *earnings;
@property (strong, nonatomic) IBOutlet UILabel *paidAmt;
@end
