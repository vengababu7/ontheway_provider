//
//  BookingViewController.h
//  iServePro
//
//  Created by Rahul Sharma on 05/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingViewController : UIViewController
@property(strong, nonatomic) NSMutableDictionary *dictBookingDetails;



@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) IBOutlet UIView *sliderOuterView;
@property (strong, nonatomic) IBOutlet UIView *sliderView;

@property (strong, nonatomic) IBOutlet UIView *middleView;

@property (strong, nonatomic) IBOutlet UILabel *custName;
@property (strong, nonatomic) IBOutlet UILabel *custAddress1;

- (IBAction)messageAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *custAddress2;
- (IBAction)callAction:(id)sender;

- (IBAction)goToGooleMaps:(id)sender;

- (IBAction)goToWazeMaps:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *wazeButton;

@property (strong, nonatomic) IBOutlet UIButton *googleButton;
@end
